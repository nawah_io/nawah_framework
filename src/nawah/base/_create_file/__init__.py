"""Provides 'create_file' Base Function callable"""

from ._funcs import create_file, set_file

__all__ = ["create_file", "set_file"]
