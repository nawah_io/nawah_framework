"""Provides 'L10N' metaclass"""


class L10N(dict):
    """L10N is metaclass to define localisation dictionary for use by Nawah App"""
