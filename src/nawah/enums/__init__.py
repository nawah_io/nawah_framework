"""Provides enums required for Nawah Fremework and apps"""

from ._enums import (
    AttrType,
    DeleteStrategy,
    Event,
    LocaleStrategy,
    NawahValues,
    VarType,
)

__all__ = [
    "AttrType",
    "DeleteStrategy",
    "Event",
    "LocaleStrategy",
    "NawahValues",
    "VarType",
]
