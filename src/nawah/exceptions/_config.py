"""Provides exceptions needed for configuring Nawah"""


class ConfigException(Exception):
    """Raised by 'config_module', 'config_app' Utility when failed to process or apply runtime
    config"""
